package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"time"
)

type akcii struct {
	name string  `json:"name"`
	fare float64 `json:"fare"`
	//count int64     `json:"count"`
	date time.Time `json:"date"`
}

type outp struct {
	start float64 `json:"start"`
	end   float64 `json:"end"`
	minzn float64 `json:"minzn"`
	maxzn float64 `json:"maxzn"`
	fl    bool
}

type tick struct {
	date time.Time `json:"date"`
	sber outp      `json:"sber"`
	amzn outp      `json:"amzn"`
	aapl outp      `json:"aapl"`
}

func copyOutp(sr outp) outp {
	var out outp
	out.end = sr.end
	out.fl = sr.fl
	out.maxzn = sr.maxzn
	out.minzn = sr.minzn
	out.start = sr.start
	return out
}

func copyTick(sr tick) tick {
	var out tick
	out.sber = copyOutp(sr.sber)
	out.aapl = copyOutp(sr.aapl)
	out.amzn = copyOutp(sr.amzn)
	out.date = sr.date
	return out
}

func checkTick(sr tick) bool {
	return sr.aapl.fl || sr.sber.fl || sr.amzn.fl
}

func dob(ak akcii, zap outp) outp {
	if zap.fl {
		if ak.fare > zap.maxzn {
			zap.maxzn = ak.fare
		}
		if ak.fare < zap.minzn {
			zap.minzn = ak.fare
		}
		zap.end = ak.fare
	} else {
		zap.end = ak.fare
		zap.start = ak.fare
		zap.maxzn = ak.fare
		zap.minzn = ak.fare
		zap.fl = true
	}
	return zap
}

func work(delta time.Duration, akc []akcii) []tick {
	timeS := 7 //часов начало

	//timeE := 0 //часов конец
	var timeP, timePE time.Time
	var outputM []tick
	var temp, clear tick
	clear.amzn.fl = false
	clear.aapl.fl = false
	clear.sber.fl = false
	

	for _, el := range akc {
		timeSt := time.Date(el.date.Year(), el.date.Month(), el.date.Day(), timeS, 0, 0, 0, time.UTC)
//проверка диапазона
		if el.date.Before(timeSt) {
			if checkTick(temp) {
				outputM = append(outputM, copyTick(temp))
			}
			temp = copyTick(clear)

		} else {
			if !((el.date.After(timeP) || el.date == timeP) && (el.date.Before(timePE) || el.date == timeP )){
				if checkTick(temp) {
					outputM = append(outputM, copyTick(temp))
					temp = copyTick(clear)
				}

				timeP = time.Date(el.date.Year(), el.date.Month(), el.date.Day(), timeS, 0, 0, 0, time.UTC)
				for timeP.Add(delta).Before(el.date) {
					timeP = timeP.Add(delta)
				}

				temp.date = timeP
				timePE = timeP.Add(delta)
			}
			switch {
			case el.name == "SBER":
				temp.sber = dob(el, temp.sber)
			case el.name == "AMZN":
				temp.amzn = dob(el, temp.amzn)
			case el.name == "AAPL":
				temp.aapl = dob(el, temp.aapl)
			}
		}
	}
	if checkTick(temp) {
		outputM = append(outputM, copyTick(temp))
	}

	return outputM
}

func write_csv(name string, zap []tick) {
	csvfile, err := os.Create(name)

	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	defer csvfile.Close()

	writer := csv.NewWriter(csvfile)
	names := []string {"AAPL","SBER","AMZN"}

	for _, el := range zap {

		ou := []outp{el.aapl, el.sber, el.amzn}

		for  i, ij:=range(ou) {

			if ij.fl {
				record := []string{names[i], el.date.Format("2006-01-02T15:04:05Z"),
					strconv.FormatFloat( ij.start, 'f',-1,64),
					strconv.FormatFloat( ij.maxzn, 'f',-1,64),
					strconv.FormatFloat( ij.minzn, 'f',-1,64),
					strconv.FormatFloat( ij.end, 'f',-1,64)}

				err := writer.Write(record)
				if err != nil {
					fmt.Println("Error:", err)
					return
				}
			}
		}

	}
	writer.Flush()
}

func main() {

	csvFile, _ := os.Open("trades.csv")
	r := csv.NewReader(bufio.NewReader(csvFile))
	var (
		akc []akcii
		fare1 float64
		ti    time.Time
	)
	layout := "2006-01-02 15:04:05"
	for{
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		fare1, _ = strconv.ParseFloat(record[1], 64)

		ti, _ = time.Parse(layout, record[3])

		akc = append(akc, akcii{
			name: record[0],
			fare: fare1,
			date: ti,
		})
	}

	min5, _ := time.ParseDuration("5m")
	min30, _ := time.ParseDuration("30m")
	min240, _ := time.ParseDuration("240m")

	write_csv("candles_5min.csv", work(min5, akc))
	write_csv("candles_30min.csv", work(min30, akc))
	write_csv("candles_240min.csv", work(min240, akc))

}
